﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioExtrato : Repositorio<Extrato>, IRepositorioExtrato
    {
        public RepositorioExtrato(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }

    public interface IRepositorioExtrato : IRepositorio<Extrato>
    {

    }
}
