﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioExternalUsers : Repositorio<ExternalUserInformation>, IRepositorioExternalUsers
    {


        public RepositorioExternalUsers(IUnitOfWork uniOfWork)
            : base(uniOfWork)
        {

        }
    }

    public interface IRepositorioExternalUsers : IRepositorio<ExternalUserInformation>
    {

    }
}
