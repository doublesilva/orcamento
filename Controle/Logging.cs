using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
//using log4net;


namespace Repositorio
{

    internal class Logger : IDisposable
    {
        StreamWriter _stream;

        public Logger(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    _stream = File.AppendText(fileName);
                }
                else
                {
                    _stream = File.CreateText(fileName);
                }
            }
            catch
            {

            }
        }

        public void WriteLog(string message)
        {
            if (_stream == null) return;

            lock (_stream)
            {
                _stream.WriteLine("=== [" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " ===");
                //_stream.WriteLine("");
                _stream.WriteLine(message);
                //_stream.WriteLine("");
                //_stream.WriteLine("---");
                _stream.WriteLine("");
                _stream.Flush();
            }
        }

        public void WriteLog(Exception e)
        {
            if (_stream == null) return;

            lock (_stream)
            {
                _stream.WriteLine("=== [" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " ===");
                _stream.WriteLine("");
                WriteExceptionData(e);
                _stream.WriteLine("");
                _stream.WriteLine("---");
                _stream.WriteLine("");
                _stream.Flush();
            }
        }

        private void WriteExceptionData(Exception e)
        {
            _stream.WriteLine("Exception " + e.GetType().FullName + ": " + e.Message);
            _stream.WriteLine("");
            _stream.WriteLine("StackTrace:");
            _stream.WriteLine(e.StackTrace);

            if (null != e.InnerException)
            {
                _stream.WriteLine("");
                _stream.WriteLine("Inner Exception:");
                _stream.WriteLine("");

                WriteExceptionData(e.InnerException);
            }
        }

        public void CloseHandle()
        {
            if (null != _stream)
            {
                _stream.Flush();
                _stream.Close();
                _stream = null;
            }
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            CloseHandle();
        }

        #endregion
    }

    public static class Logging
    {
        private static Dictionary<long, Data> _logs = new Dictionary<long, Data>();

        internal static Logger GetLoggerForTrack(long trackId)
        {
            lock (_logs)
            {
                _clearList();

                if (_logs.ContainsKey(trackId))
                {
                    _logs[trackId].Hit = DateTime.Now;
                    return _logs[trackId].Instance;
                }
                else
                {
                    string fileName = AppDomain.CurrentDomain.BaseDirectory + "Logging\\Track" + trackId.ToString() + ".log";
                    Logger fa = new Logger(fileName);

                    Data data = new Data();
                    data.Hit = DateTime.Now;
                    data.Instance = fa;

                    _logs.Add(trackId, data);

                    return fa;
                }
            }
        }

        public static void EndLogForTrack(long trackId)
        {
            try
            {
                lock (_logs)
                {
                    Data data = _logs[trackId];

                    data.Instance.CloseHandle();

                    _logs.Remove(trackId);
                }
            }
            catch { }
        }

        private static void _clearList()
        {
            long[] keys = new long[_logs.Keys.Count];
            _logs.Keys.CopyTo(keys, 0);

            for (int i = 0; i < keys.Length; i++)
            {
                Data d = _logs[keys[i]];
                if (((TimeSpan)(DateTime.Now - d.Hit)).TotalSeconds > 300)
                    EndLogForTrack(keys[i]);
            }
        }

        private class Data
        {
            public DateTime Hit;
            public Logger Instance;
        }

        public static void LogMessage(long trackId, string message)
        {
            try
            {
                Logger log = GetLoggerForTrack(trackId);
                log.WriteLog(message);
            }
            catch { }
        }

        public static void LogException(long trackId, Exception exception)
        {
            try
            {
                Logger log = GetLoggerForTrack(trackId);
                log.WriteLog(exception);
            }
            catch { }
        }
    }

}
