﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioDespesa : Repositorio<Despesa>, IRepositorioDespesa
    {
        public RepositorioDespesa(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
           
        }
    }

    public interface IRepositorioDespesa : IRepositorio<Despesa>
    {

    }
}
