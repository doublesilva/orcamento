﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioFamilia : Repositorio<Familia>, IRepositorioFamilia
    {
        public RepositorioFamilia(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }

    public interface IRepositorioFamilia : IRepositorio<Familia>
    {

    }
}
