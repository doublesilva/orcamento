﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infraestrutura;
using Dominio;

namespace Repositorio
{
    public abstract class Repositorio<TEntity> : IRepositorio<TEntity>
        where TEntity : class
    {
        private readonly IUnitOfWork _uniOfWork;
        private readonly System.Data.Entity.DbSet<TEntity> _context;
        public Repositorio(IUnitOfWork unitOfWork)
        {
            _context = ((OrcamentoContexto)unitOfWork).Set<TEntity>();
            _uniOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork { get { return _uniOfWork; } }

        public System.Data.Entity.DbSet<TEntity> Contexto { get { return _context; } }

        public TEntity Inserir(TEntity entity)
        {
            _context.Add(entity);
            return entity;
        }

        public TEntity Atualizar(TEntity entity)
        {
            ((OrcamentoContexto)_uniOfWork).Entry(entity).State = System.Data.Entity.EntityState.Modified;
            return entity;
        }

        public void Excluir(TEntity entity)
        {
            _context.Remove(entity);
        }

        public TEntity Obter(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _context;
            return query.Where(filter).FirstOrDefault();
            
        }

        public IQueryable<TEntity> ObterTodos()
        {
            return _context;
        }




        public TEntity ObterPorId(object id)
        {
            return _context.Find(id);
        }
    }
}
