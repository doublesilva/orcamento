﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioRegistro : Repositorio<Registro>, IRepositorioRegistro
    {
        public RepositorioRegistro(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
           
        }
    }

    public interface IRepositorioRegistro : IRepositorio<Registro>
    {

    }
}
