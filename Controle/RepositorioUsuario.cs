﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioUsuario : Repositorio<Usuario>, IRepositorioUsuario
    {
        public RepositorioUsuario(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        

        public bool ValidarUsuario(string email, string senha)
        {
            return this.Contexto.Any(x => x.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase) && x.Senha.Equals(senha, StringComparison.InvariantCultureIgnoreCase));
            
        }

        public bool ExisteUsuario(string email)
        {
            return this.Contexto.Any(x => x.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase));
        }
    }

    public interface IRepositorioUsuario : IRepositorio<Usuario>
    {
        bool ValidarUsuario(string email, string senha);
        bool ExisteUsuario(string email);
    }
}
