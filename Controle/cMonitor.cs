using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace Repositorio
{
    public enum eMethod
    {
        CreateRequest,
        SendRequest,
        ProcessResponse,       
        CloseConnection,
        OpenConnection,
        LoadDataBase
        
    }


    public class LogPerformance
    {
        private string _WebService;
        private eMethod _Method;
        private long communicationWait;
        private long tickStart;
        private string _TrackId;

        public LogPerformance(string className, string trackId = "0")
        {
            this._WebService = className;            
            this._TrackId = trackId;
            this.communicationWait = 0;           
        }

        public double Init(eMethod method)
        {
            this._Method = method;
            this.tickStart = DateTime.Now.Ticks;
            return this.tickStart;
        }

        public double Init()
        {            
            this.tickStart = DateTime.Now.Ticks;
            return this.tickStart;
        }

        public double EndMonitor()
        {
            communicationWait = DateTime.Now.Ticks - tickStart;
            cMonitor.newLog(this._WebService, this._Method, this.communicationWait, _TrackId);
            return this.communicationWait;
        }
    }       

    internal class cMonitor
    {
        public static void newLog(string webService, eMethod method, long communicationWait, string _TrackId)
        {
            TimeSpan tickEnd = new TimeSpan(communicationWait);
            Logging.LogMessage(0, string.Format("O Fornecedor #{0} levou {1}m:{2}s:{3}mm para realizar {4}.", webService, tickEnd.Minutes, tickEnd.Seconds, tickEnd.Milliseconds, method)); 
        }     

    }

}
