﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dominio;
using Infraestrutura;

namespace Repositorio
{
    public class RepositorioReceita : Repositorio<Receita>, IRepositorioReceita
    {
        public RepositorioReceita(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }

    public interface IRepositorioReceita : IRepositorio<Receita>
    {

    }
}
