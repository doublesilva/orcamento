﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Receita : ICloneable
    {
    
        [Key]
        public int IdReceita { get; set; }
        public int IdUsuario { get; set; }
        public int IdExtrato { get; set; }
        [DisplayFormat(DataFormatString = "{0:C2}")]
        [RegularExpression(@"^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$", ErrorMessage = "Por favor, informe um {0} válido para a receita.")]
        [Required(ErrorMessage="Por favor, informe um {0} para a receita.")]        
        public decimal Valor { get; set; }
        [Required(ErrorMessage = "Por favor, informe um {0} para a receita.")]        
        public eTipo Tipo { get; set; }
        [Display(Name="Usuário")]
        [Required(ErrorMessage = "Por favor, informe uma {0} para a receita.")]    
        public virtual Usuario Usuario { get; set; }

        public virtual Extrato Extrato { get; set; }
        [Display(Name="Descrição")]
        [Required(ErrorMessage = "Por favor, informe uma {0} para a receita.")]        
        public string Descricao { get; set; }
        [Required(ErrorMessage = "Por favor, informe uma {0} para a receita.")]        
        public DateTime Data { get; set; }
        [Display(Name="Período")]
        [Required(ErrorMessage = "Por favor, informe um {0} para a receita.")]        
        public ePeriodo Periodo { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
