﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Familia
    {
        [Key]
        public int IdFamilia
        {
            get;
            set;
        }        

        public string Nome
        {
            get;
            set;
        }

        public virtual ICollection<Usuario> Participantes
        {
            get;
            set;
        }

        public virtual ICollection<Extrato> Extratos
        {
            get;
            set;
        }
    }
}
