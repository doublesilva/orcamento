﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public enum eCategoria
    {
        Mercado = 1,
        [Display(Name="Saúde")]
        Saude = 2,
        [Display(Name = "Vestuário")]
        Vestuario = 3,
        Casa = 4,
        Celular = 6,
        Outros = 5
    }
}
