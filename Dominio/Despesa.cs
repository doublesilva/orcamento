﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Despesa : ICloneable
    {
        [Key]
        public int IdDespesa { get; set; }
        public int IdUsuario { get; set; }
        public int IdExtrato { get; set; }
        public virtual Extrato Extrato { get; set; }
        [Display(Name="Usuário")]
        [Required(ErrorMessage = "Por favor, informe uma {0} para a despesa.")]    
        public virtual Usuario Usuario { get; set; }
        [Display(Name="Descrição")]
        [Required(ErrorMessage = "Por favor, informe uma {0} para a despesa")]        
        public string Descricao { get; set; }
        [Required(ErrorMessage = "Por favor, informe uma {0} para a despesa")]        
        public DateTime Data { get; set; }
        [Display(Name="Val. Previsto")]
        [DisplayFormat(DataFormatString = "{0:C2}")]
        [RegularExpression(@"^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$", ErrorMessage="Por favor, informe um valor correto campo {0}.")]
        [Required(ErrorMessage = "Por favor, informe um {0} para a despesa")]        
        public decimal Valor { get; set; }
        [Display(Name = "Val. Pago")]
        [RegularExpression(@"^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$", ErrorMessage = "Por favor, informe um valor correto campo {0}.")]
        [DisplayFormat(DataFormatString = "{0:C2}")]     
        public decimal? Pago { get; set; }          
        public int NroParcela { get; set; }
        public int NroParcelaHaPagar { get; set; }
        [Required(ErrorMessage = "Por favor, informe um {0} para a despesa")] 
        public eTipo Tipo { get; set; }
        [Required(ErrorMessage = "Por favor, informe uma {0} para a despesa")] 
        public eCategoria Categoria { get; set; }
        [Required(ErrorMessage = "Por favor, informe um {0} para a despesa")] 
        [Display(Name="Período")]
        public ePeriodo Periodo { get; set; }

      

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
