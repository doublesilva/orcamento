﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public enum eTipo
    {
        [Display(Name="Fixo Mensal")]
        Mensal = 1,
        [Display(Name="Temporária")]
        Temporaria = 2,
        [Display(Name = "Dívida")]
        Divida = 3
    }
}
