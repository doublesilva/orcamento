﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Registro
    {
        [Key]
        public int IdConvite { get; set; }
        public int IdUsuario { get; set; }
        public eTipoRegistro Tipo { get; set; }
        public Usuario Convidado { get; set; }
        public DateTime DtEnviado { get; set; }
        public DateTime? DtAceite { get; set; }
        public bool Ativo { get; set; }
        public Guid Chave { get; set; }

    }

    public enum eTipoRegistro
    {
        Convite = 0,
        EsqueciSenha = 1
    }
}
