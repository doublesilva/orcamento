﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Usuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public int IdFamilia { get; set; }
        [Required(AllowEmptyStrings=false, ErrorMessage="Por favor, informe o seu nome")]
        public string Nome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe um email.")]
        //[RegularExpression(".+@.+\\..+", ErrorMessage = "Por favor, informe um email válido!")]
        public string Email { get; set; }
        public string Senha { get; set; }
        public bool IsAdmintrator { get; set; }
        public virtual Familia Familia { get; set; }
        public virtual ICollection<Receita> Receitas { get; set; }
        public virtual ICollection<Despesa> Despesa { get; set; }
    }
}
