﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public enum ePeriodo
    {
        [Display(Name="1º Quinzena")]
        PrimeiraQuinzena = 1,
        [Display(Name="2ª Quinzena")]
        SegundaQuinzena = 2,
        [Display(Name="Mensal")]
        Mensal = 3
    }
}
