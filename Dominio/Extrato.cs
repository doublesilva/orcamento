﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Dominio
{
    public class Extrato
    {
        [Key]
        public int IdExtrato { get; set; }
        public int IdFamilia { get; set; }
        public virtual Familia Familia { get; set; }
        public virtual ICollection<Receita> Receitas { get; set; }
        public virtual ICollection<Despesa> Despesas { get; set; }
        public DateTime Data { get; set; }
        [Display(Name="Dt. Fechamento")]
        public DateTime? DtFechamento { get; set; }   
    }
}
