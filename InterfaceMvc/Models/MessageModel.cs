﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterfaceMvc.Models
{
    public class MessageModel
    {
        public string UrlRedirect { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public TypeMessage Tipo {get; set;}
    }

    public enum TypeMessage 
    {
        ConfirmRedirect,
        ErroMessage
    }
}