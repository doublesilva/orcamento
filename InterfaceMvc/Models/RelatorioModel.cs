﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterfaceMvc.Models
{
    public class RelatorioModel
    {
        public List<ResultMonth> Months { get; set; } 
    }

    public class ResultMonth
    {
        public string Data { get; set; }        
        public Dominio.ePeriodo Periodo {get; set;}
        public decimal TotalReceita { get; set; }
        public decimal TotalDespesa { get; set; }
    }

    public class Month
    {
        public string Now { get; set; }
    }
}