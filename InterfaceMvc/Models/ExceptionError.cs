﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterfaceMvc.Models
{
    public class ExceptionError
    {
        public string Message { get; set; }
        public string Error { get; set; }
        public int StatusCode { get; set; }
    }
}