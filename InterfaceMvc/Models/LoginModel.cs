﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace InterfaceMvc.Models
{
    public class LoginModel
    {
        public string Nome { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe sua senha")]
        [StringLength(50, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Display(Name = "Usuário (Email Address)")]        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe seu usuário.")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Por favor, informe um email válido!")]
        public string Email { get; set; }

        [Display(Name = "Usuário (Email Address)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe o nome do seu orçamento.")]
        public string Familia { get; set; }

        [Required(ErrorMessage = "Por favor, repita sua senha.")]
        [StringLength(50, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirme sua Senha")]
        [Compare("Senha", ErrorMessage = "Senhas informadas não são iguais.")]
        public string ConfirmPassword { get; set; }

        public Dominio.eTipoRegistro Tipo
        {
            get;
            set;
        }
    }
}