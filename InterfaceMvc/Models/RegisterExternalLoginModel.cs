﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace InterfaceMvc.Models
{
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }

        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Display(Name = "Personal page link")]
        public string Link { get; set; }


        [Display(Name = "Usuário (Email Address)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe o nome do seu orçamento.")]
        public string Familia { get; set; }

        public bool? Verified { get; set; }
    }
}