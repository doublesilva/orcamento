﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorio;
using Dominio;
using InterfaceMvc.Models;
using System.Web.Security;
using InterfaceMvc.Code.Sercutiry;
using System.Web.Script.Serialization;
using InterfaceMvc.Code;
using InterfaceMvc.Ninject;
using Microsoft.Web.WebPages.OAuth;
using DotNetOpenAuth.AspNet;
using Facebook;

namespace InterfaceMvc.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private IRepositorioUsuario _repUsuario;
        private IRepositorioRegistro _repRegistro;
        private IRepositorioExternalUsers _repExternalUsers;
        public HomeController(IRepositorioUsuario repUsuario, IRepositorioRegistro repRegistro, IRepositorioExternalUsers repExternalUsers)
        {
            _repUsuario = repUsuario;
            _repRegistro = repRegistro;
            _repExternalUsers = repExternalUsers;
        }



        private LogPerformance log = new LogPerformance("HomeController");
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, FormCollection collection)
        {
            try
            {


                if (Membership.ValidateUser(model.Email, model.Senha))
                {

                    bool remember = collection["remember"] == "on";
                    //FormsAuthentication.SetAuthCookie(model.Email, false);
                    CreateAuthenticationTicket(model.Email);
                    string redirUrl = FormsAuthentication.GetRedirectUrl(model.Email, remember);

                    return Redirect(FormsAuthentication.DefaultUrl);

                }
                else
                {
                    ModelState.AddModelError("Email", "Usuário ou senha inválida.");
                    return View("Index", model);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Email", "Estamos verificando um erro na aplicação. Por favor, tente novamente mais tarde.");
                Logging.LogException(0, ex);
                return View("Index", model);
            }



        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        [Transaction]
        public ActionResult Register(LoginModel login, FormCollection collection)
        {


            if (_repUsuario.ExisteUsuario(login.Email))
            {
                ModelState.AddModelError("Email", "Email já existe para um cadastro ativo.");
                return View("Index", login);
            }
            Familia familia = new Familia();
            familia.Nome = login.Familia;
            Usuario usuario = new Usuario();
            usuario.Familia = familia;
            usuario.Email = login.Email;
            usuario.Senha = login.Senha;
            usuario.Nome = login.Nome;
            usuario.IsAdmintrator = true;
            _repUsuario.Inserir(usuario);

            var url = Request.Url;
            var baseurl = url.GetLeftPart(UriPartial.Authority);
            baseurl = baseurl + Url.Action(FormsAuthentication.DefaultUrl);

            EMail mailer = new EMail();
            mailer.SendMail(eMailType.Registrar, usuario.Email, new string[] { usuario.Familia.Nome }, new string[] { usuario.Nome, baseurl });
            CreateAuthenticationTicket(login.Email);

            return Redirect(FormsAuthentication.DefaultUrl);
        }

        [HttpPost]
        public ActionResult Forget(LoginModel login, FormCollection collection)
        {

            var user = _repUsuario.Obter(x => x.Email == login.Email);
            if (user != null)
            {
                Registro reg = new Registro();
                reg.Convidado = user;
                reg.Chave = Guid.NewGuid();
                reg.Ativo = true;
                reg.Tipo = eTipoRegistro.EsqueciSenha;
                reg.DtEnviado = DateTime.Now;
                var url = Request.Url;
                
                _repRegistro.Inserir(reg);
                _repRegistro.UnitOfWork.Commit();
                var baseurl = url.GetLeftPart(UriPartial.Authority);
                baseurl = baseurl + Url.Action("Aceite", "Home", new { verifycode = reg.Chave });
                EMail mailer = new EMail();
            
                mailer.SendMail(eMailType.AlterarSenha, login.Email, new string[] { user.Familia.Nome }, new string[] { baseurl });
                
            }

            return View("Index");
        }


        public void CreateAuthenticationTicket(string email)
        {
            CustomPrincipalSerializedModel serializeModel = new CustomPrincipalSerializedModel();
            log.Init(eMethod.LoadDataBase);

            log.EndMonitor();
            var authUser = _repUsuario.Obter(u => u.Email == email);
            serializeModel.Name = authUser.Nome;
            serializeModel.Email = authUser.Email;
            serializeModel.Id = authUser.IdUsuario;
            serializeModel.Familia = authUser.Familia.Nome;
            serializeModel.IsAdmin = authUser.IsAdmintrator;




            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string userData = serializer.Serialize(serializeModel);

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
              1, email, DateTime.Now, DateTime.Now.AddHours(2), true, userData);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            Response.Cookies.Add(faCookie);
        }

        public ActionResult Aceite(Guid verifycode)
        {


            Registro convite = _repRegistro.Obter(c => c.Chave == verifycode && c.Ativo);

            var login = new LoginModel();
            login.Familia = convite.Convidado.Familia.Nome;
            login.Email = convite.Convidado.Email;
            login.Nome = convite.Convidado.Nome;
            login.Tipo = convite.Tipo;
            return View(login);
        }
        [HttpPost]
        [Transaction]
        public ActionResult Confirmar(LoginModel login, FormCollection collection)
        {

            Usuario usuario = _repUsuario.Obter(u => u.Email == login.Email);

            var convites = _repRegistro.ObterTodos().Where(c => c.IdUsuario == usuario.IdUsuario);
            foreach (var item in convites)
            {
                item.Ativo = false;
                item.DtAceite = DateTime.Now;
            }
            usuario.Senha = login.Senha;
            _repUsuario.Atualizar(usuario);

            var url = Request.Url;
            var baseurl = url.GetLeftPart(UriPartial.Authority);
            baseurl = baseurl + Url.Action(FormsAuthentication.DefaultUrl);

            EMail mailer = new EMail();
            mailer.SendMail(eMailType.Registrar, usuario.Email, new string[] { usuario.Familia.Nome }, new string[] { usuario.Nome, baseurl });

            CreateAuthenticationTicket(login.Email);
            return Redirect(FormsAuthentication.DefaultUrl);
        }


        public void ExternalLogin(string provider)
        {
            // just pass a provider to make it work
            OAuthWebSecurity.RequestAuthentication(provider, Url.Action("ExternalLoginCallback"));
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(
                Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (result.ExtraData.Keys.Contains("accesstoken"))
            {
                Session["facebooktoken"] = result.ExtraData["accesstoken"];
            }

            if (result.IsSuccessful && _repUsuario.ExisteUsuario(result.UserName))
            {
           
                CreateAuthenticationTicket(result.UserName);
                string redirUrl = FormsAuthentication.GetRedirectUrl(result.UserName, false);

                return Redirect(redirUrl);
               
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(
                    result.Provider,
                    result.ProviderUserId);

                var id = result.ExtraData["id"];
                var accesstoken = result.ExtraData["accesstoken"];
                var client = new FacebookClient(accesstoken);
                bool facebookVerified;
                dynamic me = client.Get("me");
                if (me.ContainsKey("verified"))
                {
                    facebookVerified = me["verified"];
                }
                else
                {
                    facebookVerified = false;
                }
                

                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel
                {
                    UserName = me.email,
                    ExternalLoginData = loginData,
                    FullName = result.ExtraData["name"],
                    Link = result.ExtraData["link"],
                    Verified = facebookVerified
                });
            }
        }


        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel userInfo)
        {
            TempData["RegisterExternalLoginModel"] = userInfo;           
            return View(userInfo);
        }

        [HttpPost]
        [Transaction]
        public ActionResult RegisterExternal(RegisterExternalLoginModel login, FormCollection collection)        
        {

            string provider;
            string providerUserId;
            
                Familia familia = new Familia();
                familia.Nome = login.Familia;         
                Usuario usuario = new Usuario();
                usuario.Familia = familia;                
                usuario.Nome = login.FullName;
                usuario.IsAdmintrator = true;
                usuario.Email = login.UserName;
                _repUsuario.Inserir(usuario);
                _repUsuario.UnitOfWork.Commit();

                var item = TempData["RegisterExternalLoginModel"] as RegisterExternalLoginModel;
                if (OAuthWebSecurity.TryDeserializeProviderUserId(item.ExternalLoginData, out provider, out providerUserId))
                {
                    _repExternalUsers.Inserir(new ExternalUserInformation()
                    {
                        FullName = login.FullName,
                        Usuario = usuario,
                        Verified = item.Verified,
                        Link = item.Link                       
                    });
                    _repExternalUsers.UnitOfWork.Commit();
                    TempData["RegisterExternalLoginModel"] = null;
                }

                
                var url = Request.Url;
                var baseurl = url.GetLeftPart(UriPartial.Authority);
                baseurl = baseurl + Url.Action(FormsAuthentication.DefaultUrl);

                EMail mailer = new EMail();
                mailer.SendMail(eMailType.Registrar, usuario.Email, new string[] { usuario.Familia.Nome }, new string[] { usuario.Nome, baseurl });

                CreateAuthenticationTicket(login.UserName);
            return Redirect(FormsAuthentication.DefaultUrl);
       
        }
    }
}
