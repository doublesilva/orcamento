﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorio;
using Dominio;
using System.Web.Helpers;
using InterfaceMvc.Code;
using InterfaceMvc.Ninject;

namespace InterfaceMvc.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
        private IRepositorioUsuario repUsuario;
        private IRepositorioRegistro repRegistro;
        public UsuarioController(IRepositorioUsuario repUsuario, IRepositorioRegistro repRegistro)
        {
            this.repUsuario = repUsuario;
            this.repRegistro = repRegistro;
        }
        //
        // GET: /Usuario/
        //private UnitOfWork db = new UnitOfWork();
        public ActionResult List()
        {
           
                var user = repUsuario.ObterPorId((User as InterfaceMvc.Code.Sercutiry.CustomPrincipal).Id);
                return View(repUsuario.Obter(u => u.IdFamilia == user.IdFamilia));
            
        }


        public ActionResult Info()
        {

            return View(repUsuario.ObterPorId((User as InterfaceMvc.Code.Sercutiry.CustomPrincipal).Id));
            
          
        }

        public ActionResult Create()
        {
            return View();
        }

        [Transaction]
        public ActionResult Create(FormCollection collection)
        {
            
                Usuario usario = new Usuario();
                usario.Nome = collection["Nome"];
                repUsuario.Inserir(usario);

                return View("Index", repUsuario.ObterTodos());
            
        }


        public ActionResult Edit(int id = 0)
        {

            return View(repUsuario.ObterPorId(id));
            
        }


        [HttpPost, ActionName("Edit")]
        public ActionResult EditConfirm(FormCollection collection, int id = 0)
        {

               var item = repUsuario.ObterPorId(id);
                item.Nome = collection["Nome"];
                repUsuario.Atualizar(item);

                return View("List", repUsuario.ObterTodos());
            
        }


        public ActionResult Delete(int id = 0)
        {

            return View(repUsuario.ObterPorId(id));
            
        }

        [HttpPost, ActionName("Delete")]
        [Transaction]
        public ActionResult DeleteCofirmed(int id)
        {

            repUsuario.Excluir(repUsuario.ObterPorId(id));

            return View("List", repUsuario.ObterTodos());
            
        }

        public ActionResult Convite()
        {
           
                var convite = new Registro();
                return PartialView("Convite", convite);
            
        }

        [Transaction]
        public ActionResult SendInvite(Registro convite)
        {
            

                var user = (User as InterfaceMvc.Code.Sercutiry.CustomPrincipal);


                if (repUsuario.ExisteUsuario(convite.Convidado.Email))
                {
                    ModelState.AddModelError("Email", "Email já existe para um cadastro ativo.");
                    return PartialView("Convite", convite);
                }


                convite.Convidado.Familia = repUsuario.ObterPorId(user.Id).Familia;
                convite.DtEnviado = DateTime.Now;
                convite.Chave = Guid.NewGuid();
                convite.Ativo = true;
                convite.Tipo = eTipoRegistro.Convite;
                convite = repRegistro.Inserir(convite);
                
                var url = Request.Url;
                var baseurl = url.GetLeftPart(UriPartial.Authority);
                EMail mailer = new EMail();
                baseurl = baseurl + Url.Action("Aceite", "Home", new { verifycode = convite.Chave });
                mailer.SendMail(eMailType.Convidar, convite.Convidado.Email, new string[] { user.Familia }, new string[] { user.FirstName, user.Familia, baseurl });
                return RedirectToAction("List", new { id = user.Id });
            }
        

    }
}
