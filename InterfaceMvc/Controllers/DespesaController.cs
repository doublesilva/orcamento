﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorio;
using Dominio;
using InterfaceMvc.Ninject;
namespace InterfaceMvc.Controllers
{
    [Authorize]
    public class DespesaController : Controller
    {
        //
        // GET: /Despesa/

        private IRepositorioDespesa _repDespesa;
        private IRepositorioUsuario _repUsuario;
        private IRepositorioExtrato _repExtrato;
        public DespesaController(IRepositorioDespesa repDespesa, IRepositorioUsuario repUsuario, IRepositorioExtrato repExtrato)
        {
            _repDespesa = repDespesa;
            _repUsuario = repUsuario;
            _repExtrato = repExtrato;
        }

        public ActionResult Create()
        {

            var user = (User as InterfaceMvc.Code.Sercutiry.CustomPrincipal);
            ViewBag.ListPeriodo = EnumHelper.SelectListFor<ePeriodo>();
            ViewBag.ListTipo = EnumHelper.SelectListFor<eTipo>();
            ViewBag.ListCategoria = EnumHelper.SelectListFor<eCategoria>();

            ViewBag.ListUsuario = new SelectList(_repUsuario.ObterTodos(), "IdUsuario", "Nome", user.Id);
            return View();

        }

        [HttpPost]
        [Transaction]
        public ActionResult Create(Despesa despesa, FormCollection collection)
        {

            var user = (User as InterfaceMvc.Code.Sercutiry.CustomPrincipal);
            if (user.IsAdmin)
            {
                despesa.Usuario = _repUsuario.ObterPorId(Convert.ToInt32(collection["Usuario"]));
            }
            else despesa.Usuario = _repUsuario.ObterPorId(user.Id);
            DateTime apartir = despesa.Data;
            bool repetir = collection["Repetir"] == "on";
            int nrovezes = 1;

            if (repetir)
                nrovezes = Convert.ToInt32(collection["NroVezes"]);

            for (int i = 0; i < nrovezes; i++)
            {
                Despesa item = (Despesa)despesa.Clone();
                item.Data = item.Data.AddMonths(i);
                if (repetir)
                {
                    item.NroParcela = i + 1;
                    item.NroParcelaHaPagar = nrovezes;
                }
                var extrato = _repExtrato.Obter(x => x.Data.Year == item.Data.Year && x.Data.Month == item.Data.Month);

                if (extrato == null)
                {
                    extrato = new Extrato();
                    extrato.Data = item.Data;
                    item.Extrato = _repExtrato.Inserir(extrato);
                }
                _repDespesa.Inserir(item);
            }



            return RedirectToAction("Index", "Orcamento", new { Data = apartir });

        }


        public ActionResult Delete(int id)
        {

            var despesa = _repDespesa.ObterPorId(id);
            return View(despesa);

        }

        [HttpPost]
        [Transaction]
        public ActionResult Delete(FormCollection colletion, int id = 0)
        {
            var despesa = _repDespesa.ObterPorId(id);
            _repDespesa.Excluir(despesa);
            return RedirectToAction("Index", "Orcamento", new { Data = despesa.Data });

        }


        public ActionResult Edit(int id)
        {

            var despesa = _repDespesa.ObterPorId(id);
            ViewBag.ListPeriodo = EnumHelper.SelectListFor(despesa.Periodo);
            ViewBag.ListTipo = EnumHelper.SelectListFor(despesa.Tipo);
            ViewBag.ListCategoria = EnumHelper.SelectListFor(despesa.Categoria);
            return View(despesa);

        }

        [HttpPost]
        [Transaction]
        public ActionResult Save(Despesa despesa, FormCollection colletion, int id = 0)
        {

            var item = _repDespesa.ObterPorId(despesa.IdDespesa);
            var extrato = _repExtrato.Obter(x => x.Data.Year == item.Data.Year && x.Data.Month == item.Data.Month);

            if (extrato == null)
            {
                extrato = new Extrato();
                extrato.Data = item.Data;
                item.Extrato = _repExtrato.Inserir(extrato);
            }

            item.Periodo = despesa.Periodo;
            item.Valor = despesa.Valor;
            item.Pago = despesa.Pago;
            item.Tipo = despesa.Tipo;
            item.Categoria = despesa.Categoria;
            item.Data = despesa.Data;
            item.Descricao = despesa.Descricao;
            _repDespesa.Atualizar(item);

            return RedirectToAction("Index", "Orcamento", new { Data = despesa.Data });




        }
    }
}
