﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dominio;
using Repositorio;
using System.Web.Helpers;
using InterfaceMvc.Ninject;
namespace InterfaceMvc.Controllers
{
    [Authorize]
    public class OrcamentoController : Controller
    {
        //
        // GET: /Home/

        IRepositorioExtrato _repExtrato;
        IRepositorioUsuario _repUsuario;
        IRepositorioReceita _repReceita;
        IRepositorioDespesa _repDespesa;
        public OrcamentoController(IRepositorioExtrato repExtrato, IRepositorioUsuario repUsuario, IRepositorioDespesa repDespesa, IRepositorioReceita repReceita)
        {
            _repExtrato = repExtrato;
            _repUsuario = repUsuario;
            _repReceita = repReceita;
            _repDespesa = repDespesa;
        }

        public ActionResult Index()
        {
            ViewBag.ListPeriodo = EnumHelper.SelectListFor<ePeriodo>();
            return View(GetExtrato(DateTime.Now));
        }

        [HttpGet, ActionName("Index")]
        public ActionResult IndexToDate(DateTime? Data)
        {
            ViewBag.ListPeriodo = EnumHelper.SelectListFor<ePeriodo>();
            return View(GetExtrato(Data.HasValue ? Data.Value : DateTime.Now));
        }

        private Extrato GetExtrato(DateTime dt)
        {
            
                var user = _repUsuario.ObterPorId((User as InterfaceMvc.Code.Sercutiry.CustomPrincipal).Id);
                var extrato = _repExtrato.Obter(x => x.Data.Year == dt.Year && x.Data.Month == dt.Month && x.IdFamilia == user.IdFamilia);



                if (extrato != null)
                {
               
                    //var despesas = db.DespesaRepositorio.Get(y => y.IdExtrato == extrato.IdExtrato).ToList();
                    //if (despesas != null)
                    //{
                    //    despesas.ForEach(x =>
                    //    {
                    //        x.Usuario = db.UsuarioRepositorio.GetByID(x.IdUsuario);
                    //    });
                    //}
                    //else despesas = new List<Despesa>();
                    //extrato.Despesas = despesas;
                    //var receitas = db.ReceitaRepositorio.Get(x => x.IdExtrato == extrato.IdExtrato).ToList();
                    //if (receitas != null)
                    //{
                    //    receitas.ForEach(x =>
                    //    {
                    //        x.Usuario = db.UsuarioRepositorio.GetByID(x.IdUsuario);
                    //    });
                    //}
                    //else receitas = new List<Receita>();
                    //extrato.Receitas = receitas;
                }
                else extrato = new Extrato { Data = dt, Despesas = new List<Despesa>(), Receitas = new List<Receita>() };


                return extrato;
            
        }


        [HttpGet]
        public ActionResult Move(string data)
        {
            DateTime dt = Convert.ToDateTime(data);
            return PartialView("_Grids", GetExtrato(dt));
        }

        [HttpGet]
        public PartialViewResult Filter(string department, string data)
        {

            DateTime dt = Convert.ToDateTime(data);
            Extrato item = GetExtrato(dt);
            if (!string.IsNullOrEmpty(department))
            {
                ePeriodo periodo = (ePeriodo)Enum.Parse(typeof(ePeriodo), department);
                if (periodo != ePeriodo.Mensal)
                {
                    if (item.Despesas.Count > 0)
                        item.Despesas = item.Despesas.Where(d => d.Periodo == periodo).ToList();
                    if (item.Receitas.Count > 0)
                        item.Receitas = item.Receitas.Where(r => r.Periodo == periodo).ToList();
                }
            }
            return PartialView("_Grids", item);
        }


        [HttpGet]
        public PartialViewResult Pagar(int iddespesa)
        {
            
                return PartialView("_Pagar", _repDespesa.ObterPorId(iddespesa));
            
        }

        [HttpPost]
        [Transaction]
        public ActionResult PagarDespesa(int iddespesa, string valPago)
        {
            
                var despesa =_repDespesa.ObterPorId(iddespesa);

                decimal pago = 0;

                if (decimal.TryParse(valPago, out pago))
                {                    
                    despesa.Pago = pago;
                     _repDespesa.Atualizar(despesa);                    
                }
                else
                {
                    ModelState.AddModelError("Pago", "Por favor, informe um valor válido.");
                    Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                    return Content("Por favor, informe um valor válido.", System.Net.Mime.MediaTypeNames.Text.Plain);

                    // return PartialView("_Pagar", db.DespesaRepositorio.GetByID(iddespesa));
                }
                return RedirectToAction("Index", "Orcamento");
            
        }
        public ActionResult Relatorio()
        {
            
                var user = _repUsuario.ObterPorId((User as InterfaceMvc.Code.Sercutiry.CustomPrincipal).Id);
                var extratos = _repExtrato.ObterTodos().Where(x => x.IdFamilia == user.IdFamilia).ToList();

                return View(extratos);
            

        }
    }
}