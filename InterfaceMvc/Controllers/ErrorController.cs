﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InterfaceMvc.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index(int statusCode, Exception exception)
        {
            Response.StatusCode = statusCode;
            var item = new InterfaceMvc.Models.ExceptionError()
            {
                Message = exception.Message,
                Error = exception.InnerException == null ? string.Empty : exception.InnerException.ToString(),
                StatusCode = statusCode
            };

            Logging.LogException(statusCode, exception);
            return View(item);
        }

    }
}
