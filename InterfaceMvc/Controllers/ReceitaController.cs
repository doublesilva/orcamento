﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorio;
using Dominio;
using InterfaceMvc.Ninject;

namespace InterfaceMvc.Controllers
{
    [Authorize]
    public class ReceitaController : Controller
    {
        private IRepositorioReceita _repReceita;
        private IRepositorioUsuario _repUsuario;
        private IRepositorioExtrato _repExtrato;
        public ReceitaController(IRepositorioReceita repReceita, IRepositorioUsuario repUsuario, IRepositorioExtrato repExtrato)
        {
            _repReceita = repReceita;
            _repUsuario = repUsuario;
            _repExtrato = repExtrato;
        }


        //
        // GET: /Receita/

        // private UnitOfWork db = new UnitOfWork();

        public ActionResult Create()
        {

            var user = (User as InterfaceMvc.Code.Sercutiry.CustomPrincipal);
            ViewBag.ListPeriodo = EnumHelper.SelectListFor<ePeriodo>();
            ViewBag.ListTipo = EnumHelper.SelectListFor<eTipo>();
            ViewBag.ListUsuario = new SelectList(_repUsuario.ObterTodos().ToList(), "IdUsuario", "Nome", user.Id);

            return View();

        }

        [HttpPost]
        [Transaction]
        public ActionResult Create(Receita receita, FormCollection collection)
        {

            var user = (User as InterfaceMvc.Code.Sercutiry.CustomPrincipal);
            if (user.IsAdmin)
            {
                receita.Usuario = _repUsuario.Obter(u => u.IdUsuario == Convert.ToInt32(collection["Usuario"]));
            }
            else receita.Usuario = _repUsuario.Obter(u => u.IdUsuario == user.Id);
            bool repetir = collection["Repetir"] == "on";
            int nrovezes = 1;

            if (repetir)
            {
                nrovezes = Convert.ToInt32(collection["NroVezes"]);
            }

            for (int i = 0; i < nrovezes; i++)
            {
                Receita item = (Receita)receita.Clone();
                item.Data = item.Data.AddMonths(i);
                var extrato = _repExtrato.Obter(x => x.Data.Year == item.Data.Year && x.Data.Month == item.Data.Month);

                if (extrato == null)
                {
                    extrato = new Extrato();
                    extrato.Data = item.Data;
                    item.Extrato = _repExtrato.Inserir(extrato);
                }
                _repReceita.Inserir(item);
            }

            

            return RedirectToAction("Index", "Orcamento", new { Data = receita.Data });

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var receita = _repReceita.ObterPorId(id);
            return View(receita);
        }

        [HttpPost]
        [Transaction]      
        public ActionResult Delete(FormCollection colletion, int id = 0)
        {
           
                var item = _repReceita.ObterPorId(id);
                _repReceita.Excluir(item);                
                return RedirectToAction("Index", "Orcamento", new { Data = item.Data });
           
        }


        public ActionResult Edit(int id)
        {
            
                var receita = _repReceita.ObterPorId(id);                
                ViewBag.ListPeriodo = EnumHelper.SelectListFor(receita.Periodo);
                ViewBag.ListTipo = EnumHelper.SelectListFor(receita.Tipo);
                return View(receita);
            
        }

        [HttpPost]
        [Transaction]  
        public ActionResult Save(Receita receita, FormCollection colletion)
        {
            
                var item =  _repReceita.ObterPorId(receita.IdReceita);

                var extrato = _repExtrato.Obter(x => x.Data.Year == item.Data.Year && x.Data.Month == item.Data.Month);

                if(extrato == null)
                {
                    extrato = new Extrato();
                    extrato.Data = receita.Data;
                    extrato = _repExtrato.Inserir(extrato);                   

                }
                receita.Extrato = extrato;

                item.Valor = receita.Valor;
                item.Periodo = receita.Periodo;
                item.Data = receita.Data;
                item.Descricao = receita.Descricao;
                item.Tipo = receita.Tipo;
                _repReceita.Atualizar(item);

                
                return RedirectToAction("Index", "Orcamento", new { Data = item.Data });
            

        }
    }
}
