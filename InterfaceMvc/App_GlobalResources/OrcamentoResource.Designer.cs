//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "10.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class OrcamentoResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal OrcamentoResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.OrcamentoResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bem-Vindo ao orçamento familiar!!.
        /// </summary>
        internal static string BemVindo {
            get {
                return ResourceManager.GetString("BemVindo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entre.
        /// </summary>
        internal static string Entre {
            get {
                return ResourceManager.GetString("Entre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Esqueceu sua senha?.
        /// </summary>
        internal static string EsqueceuSenha {
            get {
                return ResourceManager.GetString("EsqueceuSenha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2014 &amp;copy; Desenvolvido por DoubleSilva..
        /// </summary>
        internal static string InfoCoyRight {
            get {
                return ResourceManager.GetString("InfoCoyRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe os dados abaixo:.
        /// </summary>
        internal static string InfoIncrevaSe {
            get {
                return ResourceManager.GetString("InfoIncrevaSe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to - Quer que sua família participe do seu orçamento? Aqui você tem a opção de convidar
        ///            os membros da sua família para participar do orçamento. Eles podem cadastrar novas
        ///            receitas e despesas..
        /// </summary>
        internal static string InfoPart1 {
            get {
                return ResourceManager.GetString("InfoPart1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to - Quer gerenciar o orçamento por quinzena? Aqui você também tem essa opção..
        /// </summary>
        internal static string InfoPart2 {
            get {
                return ResourceManager.GetString("InfoPart2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adicionar.
        /// </summary>
        internal static string lblAdicionar {
            get {
                return ResourceManager.GetString("lblAdicionar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelar.
        /// </summary>
        internal static string lblCancelar {
            get {
                return ResourceManager.GetString("lblCancelar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continuar.
        /// </summary>
        internal static string lblContinuar {
            get {
                return ResourceManager.GetString("lblContinuar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Convidar.
        /// </summary>
        internal static string lblConvidar {
            get {
                return ResourceManager.GetString("lblConvidar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Criar.
        /// </summary>
        internal static string lblCriar {
            get {
                return ResourceManager.GetString("lblCriar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deletar.
        /// </summary>
        internal static string lblDeletar {
            get {
                return ResourceManager.GetString("lblDeletar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descrição.
        /// </summary>
        internal static string lblDescricao {
            get {
                return ResourceManager.GetString("lblDescricao", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Despesas.
        /// </summary>
        internal static string lblDespesa {
            get {
                return ResourceManager.GetString("lblDespesa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detalhe Usuário.
        /// </summary>
        internal static string lblDetalheUsuario {
            get {
                return ResourceManager.GetString("lblDetalheUsuario", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Editar.
        /// </summary>
        internal static string lblEditar {
            get {
                return ResourceManager.GetString("lblEditar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        internal static string lblEmail {
            get {
                return ResourceManager.GetString("lblEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enviar.
        /// </summary>
        internal static string lblEnviar {
            get {
                return ResourceManager.GetString("lblEnviar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ( {0}ª de {1}).
        /// </summary>
        internal static string lblFormatParcels {
            get {
                return ResourceManager.GetString("lblFormatParcels", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lista de Participantes.
        /// </summary>
        internal static string lblListaParticipante {
            get {
                return ResourceManager.GetString("lblListaParticipante", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Por favor, aguarde um instante....
        /// </summary>
        internal static string lblLoading {
            get {
                return ResourceManager.GetString("lblLoading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nome.
        /// </summary>
        internal static string lblNome {
            get {
                return ResourceManager.GetString("lblNome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nome (Administrador).
        /// </summary>
        internal static string lblNomeAdmin {
            get {
                return ResourceManager.GetString("lblNomeAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nome Familiar.
        /// </summary>
        internal static string lblNomeFamiliar {
            get {
                return ResourceManager.GetString("lblNomeFamiliar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Orçamento dos.
        /// </summary>
        internal static string lblOrcamento {
            get {
                return ResourceManager.GetString("lblOrcamento", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pagar.
        /// </summary>
        internal static string lblPagar {
            get {
                return ResourceManager.GetString("lblPagar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Participante.
        /// </summary>
        internal static string lblParticipante {
            get {
                return ResourceManager.GetString("lblParticipante", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Período.
        /// </summary>
        internal static string lblPeriodo {
            get {
                return ResourceManager.GetString("lblPeriodo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receitas.
        /// </summary>
        internal static string lblReceita {
            get {
                return ResourceManager.GetString("lblReceita", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Relatório.
        /// </summary>
        internal static string lblRelatorio {
            get {
                return ResourceManager.GetString("lblRelatorio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Repita sua Senha.
        /// </summary>
        internal static string lblRepitaSenha {
            get {
                return ResourceManager.GetString("lblRepitaSenha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sair.
        /// </summary>
        internal static string lblSair {
            get {
                return ResourceManager.GetString("lblSair", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saldo Atual.
        /// </summary>
        internal static string lblSaldoAtual {
            get {
                return ResourceManager.GetString("lblSaldoAtual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Salvar.
        /// </summary>
        internal static string lblSalvar {
            get {
                return ResourceManager.GetString("lblSalvar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Senha.
        /// </summary>
        internal static string lblSenha {
            get {
                return ResourceManager.GetString("lblSenha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuário.
        /// </summary>
        internal static string lblUsuario {
            get {
                return ResourceManager.GetString("lblUsuario", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor.
        /// </summary>
        internal static string lblValor {
            get {
                return ResourceManager.GetString("lblValor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Val. Pago.
        /// </summary>
        internal static string lblValPago {
            get {
                return ResourceManager.GetString("lblValPago", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Val. Previsto.
        /// </summary>
        internal static string lblValPrevisto {
            get {
                return ResourceManager.GetString("lblValPrevisto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Voltar.
        /// </summary>
        internal static string lblVoltar {
            get {
                return ResourceManager.GetString("lblVoltar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lembre-me.
        /// </summary>
        internal static string LembreMe {
            get {
                return ResourceManager.GetString("LembreMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe seu email para recuperar sua senha..
        /// </summary>
        internal static string MsfInfoEsqueceuSenha {
            get {
                return ResourceManager.GetString("MsfInfoEsqueceuSenha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entre na sua conta.
        /// </summary>
        internal static string MsgEntrada {
            get {
                return ResourceManager.GetString("MsgEntrada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sem problema, clique {0} aqui {1} para recuperar sua senha..
        /// </summary>
        internal static string MsgEqueceuSenha {
            get {
                return ResourceManager.GetString("MsgEqueceuSenha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ainda não tem um orçamento familiar?.
        /// </summary>
        internal static string MsgPergCadastro {
            get {
                return ResourceManager.GetString("MsgPergCadastro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crie já o seu orçamento..
        /// </summary>
        internal static string MsgTitCadastro {
            get {
                return ResourceManager.GetString("MsgTitCadastro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inscreva-se.
        /// </summary>
        internal static string TitInscrevaSe {
            get {
                return ResourceManager.GetString("TitInscrevaSe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Meus Dados.
        /// </summary>
        internal static string TitMeusDados {
            get {
                return ResourceManager.GetString("TitMeusDados", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minha Família.
        /// </summary>
        internal static string TitMinhaFamilia {
            get {
                return ResourceManager.GetString("TitMinhaFamilia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pagar Despesa.
        /// </summary>
        internal static string TitPagarDespesa {
            get {
                return ResourceManager.GetString("TitPagarDespesa", resourceCulture);
            }
        }
    }
}
