﻿
using System.Web.Mvc;
using Infraestrutura;
namespace InterfaceMvc.Ninject
{
    public class TransactionAttribute : ActionFilterAttribute
    {
        private IUnitOfWork _unitOfWork;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.Controller.ViewData.ModelState.IsValid && filterContext.HttpContext.Error == null)
                _unitOfWork = DependencyResolver.Current.GetService<IUnitOfWork>();

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Controller.ViewData.ModelState.IsValid && filterContext.HttpContext.Error == null && _unitOfWork != null)
                _unitOfWork.Commit();

            base.OnActionExecuted(filterContext);
        }
    }
       
}