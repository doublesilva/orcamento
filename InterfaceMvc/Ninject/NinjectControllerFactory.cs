﻿using Ninject;
using System.Web.Mvc;
using Repositorio;
using Dominio;
using Infraestrutura;
using System.Web.Security;
using System.Web.Http;
using Ninject.Syntax;
namespace InterfaceMvc.Ninject
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel _ninjectKernel;
        public NinjectControllerFactory()
        {
            _ninjectKernel = new StandardKernel();
            AdBiding();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(_ninjectKernel));
        }
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, System.Type controllerType)
        {
            return controllerType == null ? null : (IController)_ninjectKernel.Get(controllerType);
        }
        private void AdBiding()
        {
            //_ninjectKernel.Bind<IFilterProvider>().To<UnitOfWorkFilter>().InSingletonScope();
           // _ninjectKernel.Bind<IUnitOfWork>().To<OrcamentoContexto>();
            _ninjectKernel.Bind<IRepositorioUsuario>().To<RepositorioUsuario>();
            _ninjectKernel.Bind<IRepositorioDespesa>().To<RepositorioDespesa>();
            _ninjectKernel.Bind<IRepositorioExtrato>().To<RepositorioExtrato>();
            _ninjectKernel.Bind<IRepositorioFamilia>().To<RepositorioFamilia>();
            _ninjectKernel.Bind<IRepositorioReceita>().To<RepositorioReceita>();
            _ninjectKernel.Bind<IRepositorioRegistro>().To<RepositorioRegistro>();
            _ninjectKernel.Bind<IRepositorioExternalUsers>().To<RepositorioExternalUsers>();
            _ninjectKernel.Bind<IUnitOfWork>().To<OrcamentoContexto>().InSingletonScope();
            //_ninjectKernel.BindFilter<UnitOfWorkFilter>(FilterScope.Action, 0).WhenActionMethodHas<TransactionAttribute>();
            _ninjectKernel.Inject(Membership.Provider);
            
            
            
        }
    }
}