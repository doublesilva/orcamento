﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using System.Web.Mvc;
using Ninject.Syntax;
using Ninject.Activation;
using Ninject.Parameters;

namespace InterfaceMvc.Ninject
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        public System.Web.Http.Dependencies.IDependencyScope BeginScope()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    /*
    * For global filters, this NinjectFilterAttributeFilterProvider is NOT used. 
    * So, we need to inject while registering global filters.
    * 
    * e.g.
    * 
    * var filter = new MessageFilterAttribute();
    * kernel.Inject(filter);
    * GlobalFilters.Filters.Add(filter);
    * 
    */
    public class NinjectFilterAttributeFilterProvider : FilterAttributeFilterProvider
    {
        private readonly IKernel _kernel;

        public NinjectFilterAttributeFilterProvider(IKernel kernel)
        {
            this._kernel = kernel;
        }

        protected override IEnumerable<FilterAttribute> GetControllerAttributes(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var attributes = base.GetControllerAttributes(controllerContext, actionDescriptor);
            foreach (var attribute in attributes)
            {
                this._kernel.Inject(attribute);
            }

            return attributes;
        }

        protected override IEnumerable<FilterAttribute> GetActionAttributes(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var attributes = base.GetActionAttributes(controllerContext, actionDescriptor);
            foreach (var attribute in attributes)
            {
                this._kernel.Inject(attribute);
            }

            return attributes;
        }
    }
}
