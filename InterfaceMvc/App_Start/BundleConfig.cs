﻿using System.Web;
using System.Web.Optimization;

namespace InterfaceMvc
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));



         //   bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/metronic").Include(
                "~/Content/themes/metronic/bootstrap/js/bootstrap.js",
                "~/Content/themes/metronic/bootstrap/js/jquery.toggle.buttons.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include(
                "~/Scripts/highcharts.js",
                "~/Scripts/modules/exporting.js"
                ));


            bundles.Add(new StyleBundle("~/Content/themes/base/css/All.css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",                        
                        "~/Content/themes/base/jquery.ui.datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/themes/metronic/bootstrap/css/All.css").Include(
                        "~/Content/themes/metronic/bootstrap/css/bootstrap.css",                        
                        "~/Content/themes/metronic/bootstrap/css/bootstrap-responsive.css"     
                        ));

            bundles.Add(new StyleBundle("~/Content/themes/metronic/css/All.css").Include(                        
                        "~/Content/themes/metronic/css/metro.css",                    
                        "~/Content/themes/metronic/css/style.css",
                        "~/Content/themes/metronic/css/style_responsive.css",
                        "~/Content/themes/metronic/css/style_default.css"                        
                        ));


            bundles.Add(new StyleBundle("~/Content/themes/metronic/font-awesome/css/All.css").Include(
                        "~/Content/themes/metronic/font-awesome/css/uniform.default.css",
                        "~/Content/themes/metronic/bootstrap/css/bootstrap-toggle-buttons.css",
                        "~/Content/themes/metronic/font-awesome/css/font-awesome.css"));

        }
    }
}