﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Dominio;
using System.Data.Entity.Validation;

namespace Infraestrutura
{
    public class OrcamentoContexto : DbContext, IUnitOfWork
    {

        private readonly Guid _sessionID = Guid.NewGuid();
        public OrcamentoContexto()
            : base("ConnDB")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Despesa> Despesas { get; set; }
        public DbSet<Extrato> Extratos { get; set; }
        public DbSet<Receita> Receitas { get; set; }
        public DbSet<Familia> Familias { get; set; }
        public DbSet<Registro> Convites { get; set; }
        public DbSet<ExternalUserInformation> ExternalUsers { get; set; }

        public int Commit()
        {
            try
            {
                return SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }        

        public void Rollback()
        {
            ChangeTracker.Entries().ToList().ForEach(entry => entry.State = EntityState.Unchanged);
        }


        public Guid SessionId
        {
            get { throw new NotImplementedException(); }
        }


        Guid IUnitOfWork.SessionId
        {
            get
            {
                return _sessionID;
            }
            
        }
    }
}
