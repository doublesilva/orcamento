namespace Infraestrutura.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExtraUserInformation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExternalUserInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdUsuario = c.Int(nullable: false),
                        FullName = c.String(maxLength: 4000),
                        Link = c.String(maxLength: 4000),
                        Verified = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .Index(t => t.IdUsuario);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExternalUserInformations", "IdUsuario", "dbo.Usuarios");
            DropIndex("dbo.ExternalUserInformations", new[] { "IdUsuario" });
            DropTable("dbo.ExternalUserInformations");
        }
    }
}
