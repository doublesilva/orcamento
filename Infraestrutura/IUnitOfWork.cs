﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infraestrutura
{
    public interface IUnitOfWork
    {
        int Commit();
        void Rollback();
        Guid SessionId { get; }
    }
}
